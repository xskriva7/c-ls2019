#ifndef MAPFIELD_H
#define MAPFIELD_H

#include <QString>
#include "unit.h"


struct MapField
{

    QString background;
    Unit* unit;
};

#endif // MAPFIELD_H
