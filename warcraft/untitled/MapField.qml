import QtQuick 2.0

Item{
    property int fieldIndex: 0

    width: main.width / 4
    height: main.height / 4

    Image {
        id: background
        source: "images/" + map.getTileBackground(fieldIndex)+ ".jpg"
        anchors.fill: parent
    }
    Image {
        id: unit
        source: ""
        anchors.fill: parent


        Component.onCompleted: {
        if(map.isOccupied(fieldIndex)){
        unit.source = "images/" + map.getTileUnit(fieldIndex)+ ".png"
        }
        }

    }





}
