#ifndef XMLPARSER_H
#define XMLPARSER_H

#include "mapfield.h"

#include <QString>
#include <QVector>



class XmlParser
{
public:
    XmlParser();
    void save(QString fileName, QVector<MapField> map);
    QVector<MapField> load(QString fileName);
};

#endif // XMLPARSER_H
