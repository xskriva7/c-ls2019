#ifndef MAP_H
#define MAP_H

#include "mapfield.h"
#include "unit.h"
#include <QObject>
#include <QVector>


class Map: public QObject
{
    Q_OBJECT
private:
    QVector<MapField> m_map;
    int m_mapWidth;

public:
    explicit Map(QObject *parent = nullptr);

    Q_PROPERTY(int width MEMBER m_mapWidth NOTIFY widthChanged())

    Q_PROPERTY(int count READ getCount NOTIFY countChanged() )

    int getCount();


    Q_INVOKABLE bool isOccupied(int index);
    Q_INVOKABLE QString getTileBackground(int index);
    Q_INVOKABLE QString getTileUnit(int index);

signals:
    void widthChanged();
    void countChanged();

};

#endif // MAP_H
