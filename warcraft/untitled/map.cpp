#include "map.h"



int Map::getCount(){
    return m_map.count();
}
Map::Map(QObject *parent):QObject(parent){
    m_mapWidth = 2;
    m_map = {
        MapField{"grass", new Unit{"knight"}},
        MapField{"grass", new Unit{"knight"}},
        MapField{"sand", nullptr},
        MapField{"grass", nullptr},
    };
}





bool Map::isOccupied(int index){
    return m_map.at(index).unit != nullptr;
}


QString Map::getTileBackground(int index){
    return m_map.at(index).background;
}


QString Map::getTileUnit(int index){
    return m_map.at(index).unit->image;
}
