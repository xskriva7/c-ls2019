//
//  FileManager.hpp
//  tile
//
//  Created by Jan Skřivánek on 06/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#ifndef FileManager_hpp
#define FileManager_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include "Tile.hpp"
#include <fstream>

class FileManager{
    public:
    

    
    
    static void saveToTextFile(const std::string filename, std::vector<std::vector<Tile>> tile );
    
    static void saveToBinaryFile(const std::string filename, std::vector<std::vector<Tile>> tile);
    
    static std::vector<std::vector<Tile>>  loadFromTextFile(const std::string filename);
    
    static std::vector<std::vector<Tile>> loadFromBinaryFile(const std::string filename);
    
    
    
};


#endif /* FileManager_hpp */
