//
//  FileManager.cpp
//  tile
//
//  Created by Jan Skřivánek on 06/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#include "FileManager.hpp"


void FileManager::saveToTextFile(const std::string filename,std::vector<std::vector<Tile>> tile ){
    std:: ofstream  out;
    out.open("data.txt");
    if (out.is_open ()) { // kontrola jestli je arraz prazdny
        
        out << tile.at(0).size()<< " "<< tile.size()<<std::endl;  // ulozeni sirka a vyska tech tile
        for ( auto tiles : tile){
            
            for (auto row  : tiles){
                out<< row.m_value <<" "<< row.m_move ;
            };
            
            out << std::endl;
            
        }
        out.close ();
    }else { std::cerr<< "Error"<<std::endl;}
    
}

void FileManager::saveToBinaryFile(const std::string filename,std::vector<std::vector<Tile>> tile ){
  /*
    std:: ofstream  out(filename , std::ios::out | std::ios:: binary );
    
    if (out.is_open ()) {for (auto  point : tile) {
        out.write((char  *)&point , sizeof(tile));}
        
        out.close ();
        
    }else {
        std::cerr  << "File  cannot  be  opened" << std::endl;}
*/
    
    
}


std::vector<std::vector<Tile>> FileManager::loadFromTextFile(const std::string filename){
/* soubor jako
    sirka vyska
    4 5 2 1
 
 
 
std:: ifstream  in;
    in.open(filename);
    if (in.is_open ()) {
        while() std::cout  << ""  << " ";
        std::cout  << std::endl;
        in.close ();
    
  */
    return std::vector<std::vector<Tile>>();
}

std::vector<std::vector<Tile>> FileManager::loadFromBinaryFile(const std::string filename){
    
    return std::vector<std::vector<Tile>>();
}
