//
//  Game.hpp
//  tile
//
//  Created by Jan Skřivánek on 06/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include <stdio.h>
#include <iostream>
#include "Tile.hpp"
#include <vector>
#include "FileManager.hpp"
class  Game{
    public:
    
    std::vector<std::vector<Tile>> m_tiles;
    std::string m_fileName;
    std::string m_binaryFileName;
    

    Game(const std::string filename,const std::string binaryFilename);
    
    
     void printTiles()const;
    
    void saveTiles(bool isBinary)const;
    void loadTiles(bool isBinary);
    void clearTiles();
    
    
};

#endif /* Game_hpp */
