		//
//  Game.cpp
//  tile
//
//  Created by Jan Skřivánek on 06/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#include "Game.hpp"
	
Game::Game(const std::string filename,const std::string binaryFilename){
    m_fileName = filename;
    m_binaryFileName = binaryFilename;
}

void Game::printTiles()const{
    for (auto row: m_tiles){
        for (auto tile : row){
            std::cout<<tile.m_value<<std::endl;
        };
    };
}

void Game::saveTiles(const bool isBinary)const{
    if (isBinary){
      FileManager::saveToTextFile(m_binaryFileName, m_tiles);
    }else {
    FileManager::saveToTextFile(m_fileName, m_tiles);
    };
    }

void Game::loadTiles(const bool isBinary){
    if (isBinary){
        FileManager::loadFromBinaryFile(m_binaryFileName);
    }else {
        FileManager::loadFromTextFile(m_fileName);
    };
}

void Game::clearTiles(){
    m_tiles.clear();
}

