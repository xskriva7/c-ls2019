//
//  main.cpp
//  tile
//
//  Created by Jan Skřivánek on 06/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#include <iostream>
#include "Game.hpp"

int main(int argc, const char * argv[]) {
    Game game("data.txt","data.bin");
    
    game.loadTiles(false);
    game.printTiles();
    game.saveTiles(true);
    game.clearTiles();
    game.printTiles();
    return 0;
}
