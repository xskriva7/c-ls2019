#ifndef TST_PRIKLAD1_H
#define TST_PRIKLAD1_H

#include "TestSuite/Suite.h"
#include "TestSuite/Test.h"
#include "priklad1.h"

// Testy: 7 b

class TestPrikladu1:public TestSuite::Test{
public:

    // otestujte, zda se vyhodi vyjimka za situace, ze predate spatny soubor - 3 b
    void testLoadFile(){
        bool control = false;
        BookShop* books = new BookShop("ordersaaa.txt");
        try {
            books->getOrder(0);
        } catch (std::out_of_range e) {
            control = true;
        }
        test_(control == true);
    }

    // otestujte, zda se povede vlozit zaznamy
    // otestujte korektni vlozeni vsech hodnot alespon 2 zaznamu 3 b
    void testGetAndInsertRecord(){
        BookShop* books = new BookShop("orders.txt");
        test_(books->countOrders(State::New) == 2);
        test_(books->countOrders(State::Shipped) == 2);
        books->insertOrder(State::New, 100, "My book", 2019, 3);
        test_(books->getOrder(6).title == "My book");
        test_(books->getOrder(6).price == 100);
        test_(books->getOrder(6).year == 2019);
        test_(books->getOrder(6).amount == 3);
        test_(books->countOrders(State::New) == 3);
        books->insertOrder(State::Shipped, 200, "My book2", 2012, 2);
        test_(books->countOrders(State::Shipped) == 3);
        test_(books->getOrder(7).title == "My book2");
        test_(books->getOrder(7).price == 200);
        test_(books->getOrder(7).year == 2012);
        test_(books->getOrder(7).amount == 2);
    }


    // otestujte, zda se povedlo seradit zaznamy
    // provedte test alespon na 3 zaznamech - 5 b
    void testSortRecords(){
        BookShop* bookShop = new BookShop("orders.txt");
        bookShop->sortByStateAndYear();
        test_(bookShop->getOrder(0).state == State::New);
        test_(bookShop->getOrder(2).state == State::Canceled);
        test_(bookShop->getOrder(0).title == "The Worst Stories");
        test_(bookShop->getOrder(4).title == "Christmas Tales");
    }

     void run(){
         testLoadFile();
         testGetAndInsertRecord();
         testSortRecords();
    }
     

};
#endif // TST_PRIKLAD1_H
