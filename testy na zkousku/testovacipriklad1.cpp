#include "priklad1.h"

BookShop::BookShop(const std::string& filename){
    try {
    loadFile(filename);
    }catch (std::invalid_argument e){
        std::cout<<"File wasnt loaded!"<<std::endl;
    }
}

void BookShop::loadFile(const std::string fileName) throw (std::invalid_argument){
     std::ifstream in;
     in.open(fileName);
     std::string state;
     float price;
     int amount;
     int year;
     std::string title;
     Order newOrder;
     if(in.is_open()){
        while(in>>state>>price>>amount>>year){
            std::getline(in,title);
            //title = title.substr(1);
            if(state == "New"){
                newOrder = Order{State::New,price,title,amount,year};
            }else if(state == "Canceled"){
                newOrder = Order{State::Canceled,price,title,amount,year};
            }else if(state == "Shipped"){
                newOrder =  Order{State::Shipped,price,title,amount,year};
            }else{
                throw std::invalid_argument("State does not exist ");
            }
            m_orders.push_back(newOrder);
        }



     }else{
         throw std::invalid_argument("Nonexistent book ");
     }


}

int BookShop::countOrders(const State state){
        return std::count_if(m_orders.begin(),m_orders.end(),[state] (Order ord)->bool{return ord.state == state;});
}

Order BookShop::getOrder(const long id) throw (std::out_of_range){
       Order order;
    try{
            order =  m_orders.at(id);
        }catch(std::out_of_range e){
            throw std::out_of_range("ID is not in vector");

        };
    return order;
}

std::vector<Order> BookShop::getOrdersFromYear(const State state, const int year) throw (std::invalid_argument){
    std::vector<Order> orders;
    for(auto order: m_orders){
        if(order.state == state && order.year==year){
            orders.push_back(order);
        }
    };
    if(orders.size() == 0){
        throw std::invalid_argument("Vector is empty");
    }
    return  orders;
}

void BookShop::insertOrder(const State state, const float price,
                           const std::string title, const int year, const int amount) {
    m_orders.push_back(Order{state,price,title,year,amount});


}

void BookShop::sortByStateAndYear(){
    std::stable_sort(m_orders.begin(),m_orders.end(),[] (Order first, Order second)->bool {return first.state<second.state;});

    std::stable_sort(m_orders.begin(),m_orders.end(),[] (Order first, Order second)->bool {return first.year<second.year;});
}

void BookShop::printInfo() {
    std::string state;
    for(auto ord:m_orders){
        if(ord.state == State::New){
           state = "New";
        }else if(ord.state == State::Canceled){
            state = "Canceled";
        }else if(ord.state == State::Shipped){
            state = "Shipped";
        }
        std::cout<< ord.title<<" "<<ord.year<<" "<<ord.price<<" "<<ord.amount<<" "<<state<<std::endl;
    };
}
