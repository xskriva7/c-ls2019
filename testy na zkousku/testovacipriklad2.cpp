#include "priklad2.h"

BookList::BookList(){

}

void BookList::testEvidenceNumber(const std::string evidenceNumber) throw (std::invalid_argument){
    std::regex reg(" (ISBN|ISSN)\\s{13}([0-9]|\\s)");
    try {
        std::regex_match(evidenceNumber, reg);
    }catch(std::invalid_argument e){
        throw std::invalid_argument("Invalid evidence number");
    }
}

void BookList::insertBook(const std::string title, const std::string evidenceNumber,
                          const float price, const int year) throw (std::invalid_argument){

    try {
        testEvidenceNumber(evidenceNumber);
        m_books.push_back(Book{title,evidenceNumber,price,year});
    }catch(std::invalid_argument e){

    throw std::invalid_argument("Invalid evidence number");
    }


}

Book BookList::getBook(const long id) throw (std::out_of_range){
    Book b;
    try {
       b =  m_books.at(id);

    }catch(std::out_of_range e){

        throw std::out_of_range("no book");
    }
    return b;
}



//************************************
// ContactBook implementace

// Konstruktor
Library::Library(const std::string name, const std::string address){
    m_name = name;
    m_address = address;
    m_bookList = new BookList;
}

// kopirovaci konstruktor - 6b
Library::Library(const Library& library){
    m_name = library.m_name;
    m_address = library.m_address;
    m_bookList = new BookList(*library.m_bookList);
}
