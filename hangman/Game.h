//
// Created by Jenda on 21.02.2019.
//

#ifndef HANGMAN_GAME_H
#define HANGMAN_GAME_H

#include <iostream>
#include <array>

class Game {
private:
    std::string word;
    int life =7;
    std::string used;
    std::array<std::string,6> good;

public:
    Game();
    void play();
private:

    char guess();
    bool check(char letter);
    void show();
    std::string getGood();
    void setGood(int pos,char letter);

};


#endif //HANGMAN_GAME_H
