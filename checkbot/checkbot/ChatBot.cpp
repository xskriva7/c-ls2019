//
//  ChatBot.cpp
//  checkbot
//
//  Created by Jan Skřivánek on 27/02/2019.
//  Copyright © 2019 default. All rights reserved.
//

#include "ChatBot.hpp"


ChatBot::ChatBot(){
    
    // da se vyrvorit jako normalni trida Reaction r = Reaction...; pak reactions.push_back(r);
    reactions.push_back(Reaction{ std::regex("school"), "boring"}); //nutno psat regex aby ten prvni string bral brat jako regex  // zkusit rozsirit a dat dam dalsi regularni vyrazy
    reactions.push_back(Reaction{ std::regex("weather"), "Is Bad "});
}

void ChatBot::play()const{
    
    // request sentence
    
    std::string userSentence = requestSentence();
    
    // kontrola
    
    while (!isQuitSentence(userSentence)){
    
        // nalezeni odpovedi
        std::string answer = searchForAnswer(userSentence);
      
        // vypsani odpovedi
        
        printAnswer(answer);
    
        // nacteni dalsi vety a opakujeme od kontroly
    
        userSentence = requestSentence();
    }
    
}

std::string ChatBot::requestSentence()const{
    std::string sentence="";
    std::cout<<"Please write your sentence:";
    getline(std::cin,sentence);
    
    return sentence;
}

std::string ChatBot::searchForAnswer(const std::string userSentence)const{
    
    for (Reaction r : reactions){  // projdu reakce a pak regex match srovnam a pokud je tak vratim odpoved z reakce
        if (std::regex_search(userSentence,r.expression )){
            return r.answer;
            
        }
            };
    
    /*
     for (auto r: reactions){
     std::smatch result;
     
     if (std::regex_search(userSentence,result,r.expression )){
     return reaction.answer;
     }
     
     }
     return "Nothing to say..." //defaultni return
     */
    
    return "Nothing to say..." ;
}

void ChatBot::printAnswer(const std::string answer)const{
    std::cout<<answer<<std::endl;
}

bool ChatBot::isQuitSentence(const std::string userSentence)const{
    
    
    return ((userSentence == "quit") or (userSentence == "exit"));
    
    //std::regex userQuitRegex("quit exit");
    //return std::regex_match(userSentence,userQuitRegex);
}


