//
//  ChatBot.hpp
//  checkbot
//
//  Created by Jan Skřivánek on 27/02/2019.
//  Copyright © 2019 default. All rights reserved.
//

#ifndef ChatBot_hpp
#define ChatBot_hpp
#include "Reaction.hpp"
#include <stdio.h>
#include <vector>
#include <iostream>
#include <regex>

class ChatBot{
private:
    std::vector<Reaction> reactions;
    
    
public:
    
    ChatBot();
    
    void play()const;
    
private:
    
    std::string requestSentence()const;
    
    std::string searchForAnswer(const std::string userSentence)const;
    
    void printAnswer(const std::string answer)const;
    
    bool isQuitSentence(const std::string userSentence)const;
    
    
    
};


#endif /* ChatBot_hpp */
