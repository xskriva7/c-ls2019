//
//  main.cpp
//  checkbot
//
//  Created by Jan Skřivánek on 27/02/2019.
//  Copyright © 2019 default. All rights reserved.
//

#include <iostream>
#include "ChatBot.hpp"
int main(int argc, const char * argv[]) {
    ChatBot* chat =new  ChatBot();
    chat->play();
    
    delete chat;
    return 0;
}
