//
//  FileManager.cpp
//  battleship
//
//  Created by Jan Skřivánek on 20/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#include "FileManager.hpp"


FileLoader::FileLoader(){
    
}

Field<Tile>* FileLoader::loadFile(std::string filename){
   
    std:: ifstream  in(filename);
    
    
    if ((in).is_open ()) {
        unsigned int width=0;
        unsigned int height=0;
        Field<Tile>* tiles = new Field<Tile>(Tile{0,false},width,height);
        
        in >> width;
        in >> height;
        int tmpVal = 0;
        for (unsigned int row= 0 ; row <height; row++ ){
            for (unsigned int column=0; column <width;column++){
                in >> tmpVal;
                tiles->insert( Tile{tmpVal,false},Position{row,column});
            
            }
            
        }
        
        
        in.close ();
        return  tiles;
    } else {
        throw std::invalid_argument("File not found!");
        
    }
    
}

/*Field<int>FileLoader::saveFile(){
    
}
*/
