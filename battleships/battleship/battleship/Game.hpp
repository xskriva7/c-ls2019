//
//  Game.hpp
//  battleship
//
//  Created by Jan Skřivánek on 20/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include <stdio.h>
#include <iostream>
#include "Field.hpp"
#include "Tile.hpp"
#include "FileManager.hpp"
class Game{
private:
    Field<Tile>* m_field1;
    Field<Tile>* m_field2;
    std::string m_player1;
    std::string m_player2;
    
public:
    
    Game(const std::string filename1,const std::string filename2);
    void play();
    
private:
    void printField(Field<Tile>* field)const;
    
    
    Field<Tile> load();
    void attack(Field<Tile> field,Position pos);
    
};

#endif /* Game_hpp */
