//
//  main.cpp
//  battleship
//
//  Created by Jan Skřivánek on 20/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#include <iostream>
#include "Field.hpp"
#include "FileManager.hpp"

int main(int argc, const char * argv[]) {
    
    Field<int>* pole = new Field<int>(0,8,8);
    pole->insert(1,Position{2,2});
    
    FileLoader* fileMan = new FileLoader();
    
    
    
    
    std::cout<<  pole->get(Position{2,2})<<std::endl;
    
    delete pole;
    delete fileMan;
    return 0;
}
