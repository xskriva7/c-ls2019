//
//  FileManager.hpp
//  battleship
//
//  Created by Jan Skřivánek on 20/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#ifndef FileManager_hpp
#define FileManager_hpp

#include <stdio.h>
#include "Field.hpp"
#include <iostream>
#include <fstream>
#include "Tile.hpp"
class FileLoader{
public:
    
    FileLoader();
    
    static Field<Tile>* loadFile(std::string filename);
   // Field<int> saveFile();
    
};
#endif /* FileManager_hpp */
