//
//  Field.hpp
//  battleship
//
//  Created by Jan Skřivánek on 20/03/2019.
//  Copyright © 2019 default. All rights reserved.
//

#ifndef Field_hpp
#define Field_hpp

#include <stdio.h>
#include <vector>

#include "Position.hpp"

template <class T>class Field {
    std::vector<std::vector<T>> m_field;
public:
    Field(const T initElement, const unsigned int width, const unsigned int height);
    void insert( T value,const Position where);
    T get(const Position where)const;
    std::size_t getRows();
    std::size_t getCols();
};



template<class T> Field<T>::Field(const T initElement, const unsigned int width, const unsigned int height){
    std::vector<T> row(width, initElement);
 //   std::vector<std::vector<T>> field(height, row);
   // m_field = field;
    
    m_field.resize(height);
    for(auto& actRow: m_field){
        actRow = row;
    }
}


template<class T> void Field<T>::insert( T value,const Position where){
    m_field.at(where.row).at(where.column) = value;
}

template<class T> T Field<T>::get(const Position where)const{
    return m_field.at(where.row).at(where.column);
}

template<class T> std::size_t Field<T>::getRows(){
    return m_field.begin();
}
template<class T> std::size_t Field<T>::getCols(){
    return m_field.end();
}

#endif /* Field_hpp */
