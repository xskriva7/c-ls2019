#include "minefieldmanager.h"

MinefieldManager::MinefieldManager(QObject *parent) : QObject(parent)
{
    m_mineField = {{0,false},{0,false},{0,false},
                   {1,false},{1,false},{1,false},
                   {1,false},{9,false},{1,false}};
}


int MinefieldManager::getMineCount(int index){
    m_mineField.at(index).revealed = true;
    return m_mineField.at(index).mineCount;
}

void MinefieldManager::saveToFile(QString path){
    QString  filePath = path.split("//").at(1);
    filePath.append("/save.json");

    QFile saveFile(filePath);

    if (! saveFile.open(QIODevice :: WriteOnly)) {
        return;
    }

    QJsonObject gameObject;
    gameObject["size"]=int(m_mineField.size());

    QJsonArray  mineArray;
    for (const auto tile:m_mineField) {


        QJsonObject  oneTile;
        oneTile["mineCount"] = tile.mineCount;
        oneTile["revealed"] = tile.revealed;

        mineArray.append(oneTile);
    }
    gameObject["mineField"] = mineArray;


    QJsonDocument  saveDoc(gameObject);
    saveFile.write(saveDoc.toJson());

    saveFile.close();

}
