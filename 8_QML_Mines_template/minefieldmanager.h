#ifndef MINEFIELDMANAGER_H
#define MINEFIELDMANAGER_H

#include <QObject>
#include <vector>
#include<iostream>
#include <QFile>
#include <QJsonObject>
#include<QJsonDocument>
#include<QJsonArray>
struct MineField{

    int mineCount;
    bool revealed;
};




class MinefieldManager : public QObject
{
    Q_OBJECT
    std::vector<MineField> m_mineField;

public:
    explicit MinefieldManager(QObject *parent = nullptr);

    Q_INVOKABLE int getMineCount(int index);
    Q_INVOKABLE void saveToFile(QString path);

signals:

public slots:
};

#endif // MINEFIELDMANAGER_H
