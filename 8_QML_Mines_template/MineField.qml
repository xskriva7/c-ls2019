import QtQuick 2.0

Rectangle {
    color: "lightgray"
    anchors.fill: parent

    Grid{
        anchors.centerIn: parent
        columns: 3
        spacing: 3

        Repeater{
            model: 9

            Mine{
                tileIndex: index
            }
        }
    }
}
