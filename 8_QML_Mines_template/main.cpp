#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "minefieldmanager.h"
#include<QQmlContext>
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    MinefieldManager minefield; // tohle vzdy pridavat brana k c++ kodu
    QQmlContext* context = engine.rootContext();
    context ->setContextProperty("manager", &minefield);
    return app.exec();
}


