//
// Created by Jenda on 26.03.2019.
//

#ifndef UNTITLED_FILEMANAGER_H
#define UNTITLED_FILEMANAGER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
class FileManager {
public:
    FileManager();

    void saveToFile(std::vector<std::string> words);

    std::vector<std::string> loadFromFile();

    void saveToContinuousFile(std::vector<std::string> words);

};


#endif //UNTITLED_FILEMANAGER_H
