//
// Created by Jenda on 26.03.2019.
//

#include "Game.h"


Game::Game() {

}

void Game::play() {
    int  mode;
    std::cout<<"New game?(1) or continue(2)";
    std::cin>> mode;
    switch (mode){

        case 1: {
            std::cout<<"Choose first word"<<std::endl;

            std::cin>>word;
            usedWords.push_back(word);

            std::cout<<"Word is "<<word<<" Choose next word"<<std::endl;
            nextWord = getWord();

            while (nextWord!= "000"){
                if (checkWord()){
                    usedWords.push_back(nextWord);
                    word = nextWord;
                }else{
                    std::cout<< "Wrong word! Try again!"<<std::endl;
                }

                std::cout<<"Word is "<<word<<"Choose next word"<<std::endl;

                nextWord= getWord();


            }

                    FileManager* fileManager = new FileManager();

                    fileManager->saveToFile(usedWords);
                break;
        }
        case 2:{
                    FileManager* fileManager = new FileManager();
                    std::vector<std::string> oldWords = fileManager->loadFromFile();

                    word = oldWords.at(oldWords.size()-1);



            std::cout<<"Word is "<<word<<"Choose next word"<<std::endl;
            nextWord = getWord();

            while (nextWord!= "000"){
                if (checkWord()){
                    usedWords.push_back(nextWord);
                    word = nextWord;
                }else{
                    std::cout<< "Wrong word! Try again!"<<std::endl;
                }

                std::cout<<"Word is "<<word<<"Choose next word"<<std::endl;

                nextWord= getWord();


            }



            fileManager->saveToContinuousFile(usedWords);
            break;
        }
        };




}

std::string Game::getWord() {
       std::string playerWord;
    std::cin>> playerWord;
    return playerWord;
}

bool Game::checkWord() {


    return nextWord.at(0) == word.at(word.size()-1);
}