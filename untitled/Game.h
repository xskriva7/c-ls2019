//
// Created by Jenda on 26.03.2019.
//

#ifndef UNTITLED_GAME_H
#define UNTITLED_GAME_H


#include <iostream>
#include <vector>
#include "FileManager.h"
class Game {
private:
    std::vector<std::string> usedWords;
    std::string word = "";
    std::string nextWord ="";
public:

    Game();

    void play();

    std::string getWord();

    bool checkWord();

};


#endif //UNTITLED_GAME_H
