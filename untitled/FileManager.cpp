//
// Created by Jenda on 26.03.2019.
//

#include "FileManager.h"

FileManager::FileManager() {

}


void FileManager::saveToFile(std::vector<std::string> words) {
    std::ofstream out ;
    out.open("input.txt",std::ios_base::app);
    if (out.is_open()){
//1.
        out << words.size()<< " ";

        for(std::string w: words){
            out << w<<" ";
        }
//2.
    /*    out << words.size()<< std::endl;
        for (int i =0;i<words.size()-1;i++){
            out << words.at(i) << " ";
        }
*/
        out.close();

    }else {
        throw std::invalid_argument("File is not open");
    }



}

std::vector<std::string> FileManager::loadFromFile() {
     std::vector<std::string> words;
     std::string word;
     int size;
    std::ifstream in;
    in.open("input.txt");
    if(in.is_open()){
        in >> size;

        for (int i = 0; i<size;i++) {
            in >> word;
            words.push_back(word);
        }

        in.close();
        return words;
    }else{throw std::invalid_argument("File not found!");}



}

void FileManager::saveToContinuousFile(std::vector<std::string> words) {

    std::ofstream out ;
    out.open("input.txt",std::ios::app);
    if (out.is_open()){

        out << words.size()<< std::endl;
        for (int i =0;i<words.size()-1;i++){
            out << words.at(i) << std::endl;
        }


        out.close();

    }else {
        throw std::invalid_argument("File is not open");
    }

}